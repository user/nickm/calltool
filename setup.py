
from distutils.core import setup

setup(name='Calltool',
      version="0.1",
      description='Callgraph generator for tor',
      author='Nick Mathewson',
      author_email='nickm@torproject.org',
      url='https://gitweb.torproject.org/user/nickm/calltool.git/',
      packages=['calltool'],
      package_dir={'': 'src'},
      license='3-clause BSD'
      )
