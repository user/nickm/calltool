#!/usr/bin/python
# This code is is dedicated to the public domain under the CC0 public domain
# dedication; see doc/cc0.txt for full information, or
#       <http://creativecommons.org/publicdomain/zero/1.0/>
# if you did not receive a copy.

__all__ = [ 'Module', 'Callgraph' ]

from logging import warn

import collections
import copy
import itertools
from calltool.Misc import iteritems

class _EmptyDict(collections.MutableMapping):
    "Always-empty dictionary that drops everything you add to it."
    def __getitem__(self, k):
        raise KeyError()
    def __setitem__(self, k, v):
        pass
    def __delitem__(self, k):
        pass
    def __iter__(self):
        return iter([])
    def __len__(self):
        return 0
    def __contains__(self, k):
        return False

MOD_VERSION = 1001

class Module(object):
    """Represents a single C file as compiled, and its callgraphs."""
    def __init__(self, name, synthetic=False, fname=None):
        """Create new module called 'name'. If 'synthetic', this module
           doesn't actually have a C file of its own yet."""
        self._version = MOD_VERSION
        self._name = name
        self._fname = fname
        self._defined = set() # set of defined symbols.
        self._uses = {} # map from function name to used symbol
        self._mentions = {} # as uses, but only for symbols that are loaded.
        self._allUsed = None # set of all used symbols; lazy-built.
        self._isSynthetic = synthetic # True if we haven't seen a C file for this
        self._inGroups = set([name]) # Which module-groups is this one in?
        self._isExternal = False # Is this a library?

    def _invalidate(self):
        self._allUsed = None

    def getName(self):
        """Return this module's (human-readble) name."""
        return self._name

    def getFname(self):
        """DOCDOC"""
        return self._fname

    def addToGroup(self, groupName):
        """Note that this module belongs to a group called 'groupName'."""
        self._inGroups.add(groupName)

    def getGroups(self):
        """Return a set of the names of all the groups this module is in."""
        return self._inGroups

    def addDefinition(self, function):
        """Note that a definition for 'function' appears in this module."""
        self._defined.add(function)

    def getDefinitions(self):
        """Return the  set of symbols defined in this function."""
        return self._defined

    def addUse(self, used, user="*", isCall=True):
        """Note that the symbol 'used' is used in this module. If 'user' is
           provided, it is the function that uses that symbol."""
        self._allUsed = None
        if isCall:
            d_ref = self._uses
        else:
            d_ref = self._mentions

        d_ref.setdefault(user, set()).add(used)

    def mergeMentions(self):
        for user, usedSet in iteritems(self._mentions):
            self._uses.setdefault(user, set()).update(usedSet)

    def makeExternal(self):
        """Note that this module is external, and we're not going to
           follow the callgraph through it.  External modules are mostly
           libraries."""
        self._isExternal = True
        self._uses = _EmptyDict()

    def isExternal(self):
        """Return true iff this module is external"""
        return self._isExternal

    def getAllUsed(self):
        """Return a set of all symbols used from this module."""
        if self._allUsed is None:
            u = reduce(set.union, self._uses.itervalues(), set())
            self._allUsed = frozenset(u)
        return self._allUsed

    def moveFuncToModule(self, fn, otherModule):
        """Move the function 'fn' from this module to 'othermodule'."""
        self._invalidate()
        otherModule._invalidate()
        otherModule._defined.add(fn)
        otherModule._uses[fn] = self._uses[fn]
        self._defined.remove(fn)
        del self._uses[fn]

    def isSynthetic(self):
        """Return true iff this module is synthetic."""
        return self._isSynthetic

    def dump(self):
        print("Declares: %s" % (" ".join(sorted(self._defined))))
        for fn in sorted(self._uses.keys()):
            print("%s calls: %s" %(fn, " ".join(sorted(self._uses[fn]))))

WARNED_ALREADY = set()

class Callgraph(object):
    """A callgraph of functions and modules."""
    def __init__(self):
        self._modules = [] # List of modules
        self._modulesByName = {} # Map from module name to module.
        self._locationMap = None # Map from symbol name to module providing it.

    def addModule(self, mod):
        """Add a new module to the callgraph."""
        if mod.getName() in self._modulesByName:
            warn("Multiple modules are named %s", mod.getName())
        self._modules.append(mod)
        self._modulesByName[mod.getName()]=mod

    def getModule(self, modname):
        """Return the module named 'modname'."""
        return self._modulesByName[modname]

    def setModuleFname(self, fname):
        """DOCDOC"""
        self._moduleFname = fname

    def getOrCreateModule(self, modname):
        """Return the module named 'modname'; if none exists, create and
           return a new synthetic module with that name."""
        try:
            return self.getModule(modname)
        except KeyError:
            mod = Module(modname, synthetic=True)
            self.addModule(modname)
            return mod

    def mergeMentions(self):
        for m in self._modules:
            m.mergeMentions()

    def getModuleDefining(self, symbol):
        """Return the name of the module in which 'symbol' is defined."""
        return self._locationMap.get(symbol)

    def rebuildLocationMap(self):
        """Reconstruct the location map. Warn if we can't map a symbol
           uniquely."""
        d = {}
        for mod in self._modules:
            for sym in mod.getDefinitions():
                if sym in d:
                    if mod.isExternal():
                        continue
                    if not d[sym].isExternal():
                        warn("%s is declared in both %s and %s!",
                             sym, d[sym].getName(), mod.getName())
                d[sym] = mod
        self._locationMap = d

    def buildModuleCalls(self):
        """Return a dict where the keys are the module names, and each value
           is a dict describing the modules it uses.  Each such
           value-dict maps the name of used modules to a set of
           (user_function, used_symbol).

           Warn whenever something uses a symbol we can't find a definiton for.
        """
        # kludge -- ugly return value.
        # map( using mod -> map ( used mod : set( (user_fn, used_fn)) )

        self.rebuildLocationMap()
        modcalls = { }
        for mod in self._modules:
            modName = mod.getName()
            modUses = modcalls.setdefault(modName, {})
            for user, usedset in iteritems(mod._uses):
                for used in usedset:
                    try:
                        usedMod = self._locationMap[used]
                    except KeyError:
                        if used not in WARNED_ALREADY:
                            warn("%s:%s uses %s, but where is that defined?",
                                 modName, user, used)
                            WARNED_ALREADY.add(used)
                    else:
                        usedName = usedMod.getName()
                        modUses.setdefault(usedName, set()).add((user, used))

        simplified = { }
        for mod, calls in iteritems(modcalls):
            simplified[mod] = set(calls.keys())

        return modcalls, simplified

    def getFunctionCalls(self):
        """
           Return dict mapping function to set of functions it calls.
        """

        result = {}
        for mod in self._modules:
            for user, usedset in iteritems(mod._uses):
                result.setdefault(user, set()).update(usedset)

        return result
